package teame.milestone;

import teame.milestone.model.Milestone;

import java.util.ArrayList;
import java.util.List;

public class Planner {

    private String plannerName;
    private List<Milestone> milestones;

    public Planner() { milestones = new ArrayList<>(); }

    public void addMilestone(Milestone m) {
        milestones.add(m);
    }

    public List<Milestone> getMilestones() {
        return milestones;
    }
}
