package teame.milestone.servlet;

import teame.milestone.db.MySqlUser;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;
import teame.milestone.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;

public class UserServlet extends BaseServlet {

    private static final long serialVersionUID = 687117339002032958L;

    private final MySqlUser sql;

    public UserServlet(MySqlUser msu) {
        this.sql = msu;
    }

    // returns true if user exists or false if not exists
    public boolean userExists(String username) {
        teame.milestone.model.User user = sql.getUserByUsername(username);
        if (user != null){
            return true;
        }else{
            return false;
        }
    }

    // used to let user check if they have correct user and pass before making other requests
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        JSONObject resJson = new JSONObject();

        String username = request.getHeader("username");
        String password = request.getHeader("password");
        String passwordHash = null;
        if(password != null){
            passwordHash = getSHA256(password);
        }

        if( username != null && password != null && userAuth(username, passwordHash) != null){
            resJson.put("username",username);
            resJson.put("passwordHash",passwordHash);
            resJson.put("success",true);
        } else{
            resJson.put("username",null);
            resJson.put("passwordHash",null);
            resJson.put("success",false);
        }

        response.addHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("application/json");
        response.setStatus(200);
        response.getOutputStream().write(resJson.toString().getBytes(Charset.forName("utf-8")));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        JSONParser parser = new JSONParser();
        JSONObject reqJson;
        JSONObject resJson = new JSONObject();
        boolean reqError = false;

        String username = null;
        String passwordHash = null;

        try{
            Object obj = parser.parse(parseBody(request));
            reqJson = (JSONObject)obj;
            if(reqJson.get("username") != null && reqJson.get("password") != null ) {
                username = reqJson.get("username").toString();
                passwordHash = getSHA256(reqJson.get("password").toString());
            }else{
                reqError = true;
            }
        }catch(ParseException pe){
            reqError = true;
        }

        if(!reqError && !userExists(username)) {
            resJson.put("username",username);
            resJson.put("passwordHash",passwordHash);
            resJson.put("success",true);
            resJson.put("alreadyExists",false);
            User user = new User(0 ,username, passwordHash);
            sql.addUser(user);
        }else if(reqError){
            resJson.put("username",null);
            resJson.put("passwordHash",null);
            resJson.put("success",false);
            resJson.put("alreadyExists",false);
        }else{
            resJson.put("username",null);
            resJson.put("passwordHash",null);
            resJson.put("success",false);
            resJson.put("alreadyExists",true);
        }

        response.addHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("application/json");
        response.setStatus(200);
        response.getOutputStream().write(resJson.toString().getBytes(Charset.forName("utf-8")));

    }
}
