package teame.milestone.servlet;

import teame.milestone.db.MySqlMilestone;
import teame.milestone.Planner;
import teame.milestone.model.Milestone;
import teame.milestone.mustache.MustacheRenderer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

public class MilestoneServlet extends BaseServlet {

    private static final String MESSAGE_BOARD_TEMPLATE = "ms.mustache";
    private static final long serialVersionUID = 687117339002032958L;

    private final MustacheRenderer mr;
    private Planner p;
    private final MySqlMilestone sql;

    public MilestoneServlet(MySqlMilestone msm) {
        mr = new MustacheRenderer();
        this.sql = msm;
    }

    private Planner getPlanner() {
        List<Milestone> milestones = sql.findMilestones();

        p = new Planner();

        for (Milestone ms : milestones) {
            p.addMilestone(ms);
        }
        return p;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String html = mr.render(MESSAGE_BOARD_TEMPLATE, getPlanner());
        response.setContentType("text/html");
        response.setStatus(200);
        response.getOutputStream().write(html.getBytes(Charset.forName("utf-8")));
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("pname");
        String desc = request.getParameter("pdesc");
        String due = request.getParameter("pdue");
        String comp = request.getParameter("pcomp");
        teame.milestone.model.Milestone m = new teame.milestone.model.Milestone(name, desc, due, comp);
        sql.addMilestone(m);
        response.sendRedirect("/milestone");
    }
}
