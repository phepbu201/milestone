
package teame.milestone.servlet;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import teame.milestone.db.MySqlMilestone;
import teame.milestone.db.MySqlUser;
import teame.milestone.db.MySqlPlanner;

public class Runner {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(Runner.class);

    private static final int PORT = 9000;
    private final MySqlMilestone sqlMilestone;
    private final MySqlUser sqlUser;
    private final MySqlPlanner sqlPlanner;

    private Runner() {
        sqlMilestone = new MySqlMilestone();
        sqlUser = new MySqlUser();
        sqlPlanner = new MySqlPlanner();
    }

    private void start() throws Exception {
        Server server = new Server(PORT);

        ServletContextHandler handler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);

        handler.setContextPath("/");
        handler.setInitParameter("org.eclipse.jetty.servlet.Default." + "resourceBase", "src/main/resources/webapp");

        handler.addServlet(new ServletHolder(new MilestoneServlet(sqlMilestone)), "/milestone");
        handler.addServlet(new ServletHolder(new UserServlet(sqlUser)), "/user");
        handler.addServlet(new ServletHolder(new PlannerServlet(sqlPlanner)), "/planner");

        DefaultServlet ds = new DefaultServlet();
        handler.addServlet(new ServletHolder(ds), "/");

        server.start();
        LOG.info("Server started, will run until terminated");
        server.join();

    }

    public static void main(String[] args) {
        try {
            LOG.info("starting");
            new Runner().start();
        } catch (Exception e) {
            LOG.error("Unexpected error running milestone: " + e.getMessage());
        }
    }
}
