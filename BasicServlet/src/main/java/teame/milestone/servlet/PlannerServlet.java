package teame.milestone.servlet;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import teame.milestone.db.MySqlPlanner;
import teame.milestone.model.Planner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Iterator;

public class PlannerServlet extends BaseServlet {
    private static final long serialVersionUID = 685117339002032258L;

    private final MySqlPlanner sql;

    public PlannerServlet(MySqlPlanner msp) {
        this.sql = msp;
    }

    // used to let user check if they have correct user and pass before making other requests
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        JSONObject resJson = new JSONObject();
        JSONParser parser = new JSONParser();
        JSONObject reqJson;
        boolean reqError = false;

        String username = request.getHeader("username");
        String passwordHash = request.getHeader("passwordHash");
        teame.milestone.model.User user = null;
        if(username != null && passwordHash != null){
            user = userAuth(username, passwordHash);
        }

        List<Planner> plannerList = null;
        JSONArray plannerListJson = new JSONArray();
        if(user != null) {// if user found with pass and name given try to parse request and add planner
            plannerList = sql.getPlannersByUserId(user.getUserId());
            Iterator<Planner> iter = plannerList.iterator();
            while(iter.hasNext()){
                Planner temp = iter.next();
                JSONObject plannerObj = new JSONObject();
                plannerObj.put("id",temp.getId());
                plannerObj.put("name",temp.getName());
                plannerListJson.add(plannerObj);
            }
        }else{
            reqError = true;
        }
        resJson.put("planners", plannerListJson);

        response.addHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("application/json");
        response.setStatus(200);

        if(!reqError) {
            resJson.put("success",true);
            response.getOutputStream().write(resJson.toString().getBytes(Charset.forName("utf-8")));
        }else{
            resJson.put("success",false);
            response.getOutputStream().write(resJson.toString().getBytes(Charset.forName("utf-8")));
        }

    }

    // put will be used to do edits since update is not there
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        JSONObject resJson = new JSONObject();
        JSONParser parser = new JSONParser();
        JSONObject reqJson;
        boolean reqError = false;

        String username = request.getHeader("username");
        String passwordHash = request.getHeader("passwordHash");
        teame.milestone.model.User user = null;
        if(username != null && passwordHash != null){
            user = userAuth(username, passwordHash);
        }

        String plannerName = null;
        if(user != null) {// if user found with pass and name given try to parse request and add planner
            try{// could turn this into function
                Object obj = parser.parse(parseBody(request));
                reqJson = (JSONObject)obj;
                if(reqJson.get("name") != null) {
                    plannerName = reqJson.get("name").toString();
                    teame.milestone.model.Planner planner = sql.addPlanner(plannerName,user.getUserId());
                    resJson.put("id",planner.getId());
                    resJson.put("name",planner.getName());
                }else{
                    reqError = true;
                }
            }catch(ParseException pe){
                reqError = true;
            }
        }else{
            reqError = true;
        }

        response.addHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("application/json");
        response.setStatus(200);

        if(!reqError) {
            resJson.put("success",true);
            response.getOutputStream().write(resJson.toString().getBytes(Charset.forName("utf-8")));
        }else{
            resJson.put("success",false);
            response.getOutputStream().write(resJson.toString().getBytes(Charset.forName("utf-8")));
        }

    }
}
