// Copyright (c) 2018 Katrin Hartmann. All Rights Reserved.
//
// File:        MemMilestones.java
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package teame.milestone.db;

import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import teame.milestone.model.Milestone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class MemMilestones implements IMilestoneDB {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(MemMilestones.class);

    private final List<Milestone> Milestones;
    private long index;

    public MemMilestones() {
        Milestones = new ArrayList<>();
        index = 0;
    }

    @Override
    public synchronized Milestone get(long id) {
        for (Milestone m: Milestones) {
            if (m.getId() == id) {
                return m;
            }
        }
        return null;
    }

    @Override
    public synchronized List<Milestone> list() {
        return Collections.unmodifiableList(Milestones);
    }

    @Override
    public synchronized void add(@NonNull String name, String desc, String due, String comp, String user) {
        Milestone m = new Milestone(index++, name, desc, due, comp, user);
        Milestones.add(m);
    }

    @Override
    public synchronized List<Milestone> user(@NonNull String user) {
        List<Milestone> out = Milestones.stream().filter(m -> user.equals(m.getUser())).collect(Collectors.toList());
        return Collections.unmodifiableList(out);
    }

    @Override
    public synchronized void delete(long id) {
        for (Milestone m: Milestones) {
            if (id == m.getId()) {
                Milestones.remove(m);
                return;
            }
        }
    }
}
