package teame.milestone.db;

import teame.milestone.model.Planner;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlPlanner implements AutoCloseable {
    private Connection connection;

    public MySqlPlanner() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://pimih.com:3306/milestone", "root", "Team_15_Pass");
        } catch (ClassNotFoundException | SQLException e) { e.printStackTrace(); } // if error print stack trace
    }

    public teame.milestone.model.Planner addPlanner(String name, int userId) {
        //retunr generated keys makes this give result set with the id of just added row
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO planners (name, userId) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, name);
            ps.setString(2, Integer.toString(userId));
            ps.execute(); // execute statement
            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next()) {
                return new teame.milestone.model.Planner(rs.getInt(1),name,userId);
            }
        } catch (SQLException e) { e.printStackTrace(); } // if error print stack trace
        return null;
    }

    public void deletePlannerById(int id) {
        try (PreparedStatement ps = connection.prepareStatement("DELETE FROM planners WHERE id = (?)")) {
            ps.setString(1, Integer.toString(id));
            ps.execute(); // execute statement
        } catch (SQLException e) { e.printStackTrace(); } // if error print stack trace
    }

    public List<teame.milestone.model.Planner> getPlannersByUserId(int userId) {
        List<teame.milestone.model.Planner> plannerList = new ArrayList<>(); // declare array list that will be filled with milestones and returned
        try (PreparedStatement ps = connection.prepareStatement("SELECT id, name, userId FROM planners WHERE userId = (?)")) {
            ps.setString(1, Integer.toString(userId));
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                plannerList.add(new teame.milestone.model.Planner(
                        Integer.parseInt(resultSet.getString(1)),
                        resultSet.getString(2),
                        Integer.parseInt(resultSet.getString(3))
                ));
            }
        } catch (SQLException e) { e.printStackTrace(); } // if error print stack trace
        return plannerList;
    }

    @Override
    public void close() { // function auto closes connection
        try {
            if (connection != null) {
                connection.close();;
                connection = null;
            }
        } catch (SQLException e) { e.printStackTrace(); } // if error print stack trace
    }

}
