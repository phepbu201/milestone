package teame.milestone.db;
import teame.milestone.model.Milestone;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlMilestone implements AutoCloseable {

    private Connection connection;

    public MySqlMilestone() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://pimih.com:3306/milestone", "root", "Team_15_Pass");
        } catch (ClassNotFoundException | SQLException e) { e.printStackTrace(); } // if error print stack trace
    }

    public void addMilestone(Milestone ms) {
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO milestones (name, description, dueDate, completionDate, plannerId) VALUES (?,?,?,?,?)")) {
            ps.setString(1, ms.getName()); // this and next 4 lines fill in question marks in above preped statement
            ps.setString(2, ms.getDescription());
            ps.setString(3, ms.getDueDate());
            ps.setString(4, ms.getCompletionDate());
            ps.setString(5, "1");
            ps.execute(); // execute statement
        } catch (SQLException e) { e.printStackTrace(); } // if error print stack trace
    }

    public List<Milestone> findMilestones() { // function returns list of milestones
        List<Milestone> milestoneList = new ArrayList<>(); // declare array list that will be filled with milestones and returned
        try (PreparedStatement ps = connection.prepareStatement("SELECT name, description, dueDate, completionDate  FROM milestones")) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                milestoneList.add(new teame.milestone.model.Milestone(resultSet.getString(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4)));
            }
        } catch (SQLException e) { e.printStackTrace(); } // if error print stack trace
        return milestoneList;
    }

    @Override
    public void close() { // function auto closes connection
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) { e.printStackTrace(); } // if error print stack trace
    }
}