package teame.milestone.db;

import teame.milestone.model.User;

import java.sql.*;

public class MySqlUser implements AutoCloseable {
    private Connection connection;

    public MySqlUser() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://pimih.com:3306/milestone", "root", "Team_15_Pass");
        } catch (ClassNotFoundException | SQLException e) { e.printStackTrace(); } // if error print stack trace
    }

    public void addUser(User user) {
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO users (username, passwordHash) VALUES (?,?)")) {
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPasswordHash());
            ps.execute(); // execute statement
        } catch (SQLException e) { e.printStackTrace(); } // if error print stack trace
    }

    // checks if user exists with pass and username. used to authenticate
    public teame.milestone.model.User getUser(String username, String passwordHash) {
        teame.milestone.model.User user = null;
        try (PreparedStatement ps = connection.prepareStatement("SELECT id, username, passwordHash FROM users WHERE username = (?) AND  passwordHash = (?)")) {
            ps.setString(1, username);
            ps.setString(2, passwordHash);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                user = new teame.milestone.model.User(
                        Integer.parseInt(resultSet.getString(1)),
                        resultSet.getString(2),
                        resultSet.getString(3)
                );
            }
        } catch (SQLException e) { e.printStackTrace(); } // if error print stack trace
        return(user);
    }

    // checks if user exists with username. used to make sure duplicate users arent created
    public teame.milestone.model.User getUserByUsername(String username) {
        teame.milestone.model.User user = null;
        try (PreparedStatement ps = connection.prepareStatement("SELECT id, username, passwordHash FROM users WHERE username = (?)")) {
            ps.setString(1, username);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                user = new teame.milestone.model.User(
                        Integer.parseInt(resultSet.getString(1)),
                        resultSet.getString(2),
                        resultSet.getString(3)
                );
            }
        } catch (SQLException e) { e.printStackTrace(); } // if error print stack trace
        return(user);
    }

    @Override
    public void close() { // function auto closes connection
        try {
            if (connection != null) {
                connection.close();
                connection = null;;
            }
        } catch (SQLException e) { e.printStackTrace(); } // if error print stack trace
    }

}
