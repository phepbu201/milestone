
package teame.milestone.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import teame.milestone.model.Milestone;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class H2Milestone extends H2Base implements IMilestoneDB {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(H2Milestone.class);

    public H2Milestone(ConnectionSupplier connectionSupplier) {
        super(connectionSupplier.provide());
        try {
            initialise(getConnection());
        } catch (Exception e) {
            LOG.error("Can't find database driver: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
    private void initialise(Connection conn) throws SQLException {
        execute(conn, "CREATE TABLE IF NOT EXISTS " +
                "milestones (id BIGINT AUTO_INCREMENT, name VARCHAR(255), description VARCHAR(255), " +
                "dueDate VARCHAR(255), completionDate VARCHAR(255), user VARCHAR(255), PRIMARY KEY(id))");
    }

    @Override
    public Milestone get(long id) {
        String ps = "SELECT id, name, description, dueDate, completionDate, user FROM milestones where id = ?";
        Connection conn = getConnection();
        try {
            PreparedStatement p = conn.prepareStatement(ps);
            ResultSet rs = p.executeQuery();
            if(rs.next()) {
                return rs2milestone(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new H2MilestonesException(e);
        }
    }

    @Override
    public void add(String name, String description, String dueDate, String completionDate, String user) {
        final String ADD_MILESTONE_QUERY = "INSERT INTO milestones (name, description, dueDate, completionDate, user) VALUES (?,?,?,?,?)";
        Connection connection = getConnection();
        try (PreparedStatement ps = connection.prepareStatement(ADD_MILESTONE_QUERY)) {
            ps.setString(1, name);
            ps.setString(2, description);
            ps.setString(3, dueDate);
            ps.setString(4, completionDate);
            ps.setString(5, user);
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Milestone> list() {
        final String LIST_MILESTONES_QUERY = "SELECT id, name, description, dueDate, completionDate, user  FROM milestones";
        Connection connection = getConnection();
        List<Milestone> out = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(LIST_MILESTONES_QUERY)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                out.add(new Milestone(rs.getLong(1), rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6)));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return out;
    }

    @Override
    public List<Milestone> user(String user) {
        String ps = "SELECT id, name, description, dueDate, completionDate, user FROM milestones WHERE user = ?";
        Connection conn = getConnection();
        try {
            List<Milestone> out = new ArrayList<>();
            PreparedStatement p = conn.prepareStatement(ps);
            p.setString(1, user);
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                Milestone m = rs2milestone(rs);
                out.add(m);
            }
            return out;
        } catch (SQLException e) {
            throw new H2MilestonesException(e);
        }
    }

    @Override
    public void delete(long id) {
        Connection conn = getConnection();
        String ps = "DELETE FROM milestones WHERE id = ?";
        try (PreparedStatement p = conn.prepareStatement(ps)) {
            p.setLong(1, id);
            p.execute();
        } catch (SQLException e) {
            throw new H2MilestonesException(e);
        }
    }

    private static Milestone rs2milestone(ResultSet rs) throws SQLException {
        return new Milestone(rs.getLong(1), rs.getString(2),
                rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
    }

    public static final class H2MilestonesException extends RuntimeException {
        H2MilestonesException(Exception e) {
            super(e);
        }
    }
}
