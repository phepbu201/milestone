package teame.milestone.model;

public class User {
    private int userId;
    private String username;
    private String passwordHash;

    public User(int userId, String username, String passwordHash) {
        this.userId = userId;
        this.username = username;
        this.passwordHash = passwordHash;
    }

    public int getUserId() {
        return this.userId;
    }
    public String getUsername() {
        return this.username;
    }
    public String getPasswordHash() {
        return this.passwordHash;
    }

    public void setUserId(int userId) { this.userId = userId; }
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPasswordHash(String passwordHash) {
        this.username = passwordHash;
    }
}
