package teame.milestone.model;

public class MilestoneList {
    private String name;
    private String milestoneIdList;
    private String link;
    private String userId;

    public MilestoneList(String name, String milestoneIdList, String link, String userId) {
        this.name = name;
        this.milestoneIdList = milestoneIdList;
        this.link = link;
        this.userId = userId;
    }

    public String getName() {
        return this.name;
    }
    public String getMilestoneIdList() {
        return this.milestoneIdList;
    }
    public String getLink() {
        return this.link;
    }
    public String getUserId() {
        return this.userId;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setMilestoneIdList(String milestoneIdList) {
        this.milestoneIdList = milestoneIdList;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
