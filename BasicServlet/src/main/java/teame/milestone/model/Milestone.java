package teame.milestone.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Milestone {

    private String name;
    private String description;
    private String dueDate;
    private String completionDate;

    public Milestone() {

    }

    public Milestone(String n, String d, String due, String comp) {
        this.name = n;
        this.description = d;
//        try {
//            this.dueDate = new SimpleDateFormat("dd/MM/yyyy").parse(due);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        try {
//            this.completionDate = new SimpleDateFormat("dd/MM/yyyy").parse(comp);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        this.dueDate = due;
        this.completionDate = comp;
    }

    public String getName() {
        return this.name;
    }
    public String getDescription() {
        return this.description;
    }
    public String getDueDate() { return this.dueDate; }
    public String getCompletionDate() { return this.completionDate; }

    public void setName(String nm) {
        this.name = nm;
    }
}
