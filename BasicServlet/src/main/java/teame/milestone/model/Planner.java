package teame.milestone.model;

public class Planner {
    private String name;
    private int userId;
    private int id;

    public Planner(int id, String name, int userId) {
        this.id = id;
        this.name = name;
        this.userId = userId;
    }

    public int getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
    public int getUserId() {
        return this.userId;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setName(String username) {
        this.name = username;
    }
    public void setUserId(int userId) {
            this.userId = userId;
        }
}
