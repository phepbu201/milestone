CREATE TABLE IF NOT EXISTS milestones (
  id int AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255),
  description VARCHAR(255),
  dueDate VARCHAR(255),
  completionDate VARCHAR(255)
);