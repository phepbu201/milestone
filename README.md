# WPD2 MilestonePlanner

This is a milestone planner created for the web platform development 2 module.

## Getting Started

To get a copy of this project up and running clone this repository and use intellij to run the main method in ./milestone/BasicServlet/src/main/java/teame/milestone/Runner.java

### Prerequisites

In order to clone this repository you must first have git command line software installed such as gitBash - available at :

```
https://gitforwindows.org/ 
```
The easiest way to run the program is to open it through an IDE such as IntelliJ IDEA, availbale at:

```
https://www.jetbrains.com/idea/
```

### Installing

To clone the repository, open up your commandline with git installed and use the command

```
git clone https://<Username>@bitbucket.org/phepbu201/milestone.git
```

Once the repository has finished installing on your machine, use your IDE (Such as intelliJ) to open the project folder then run the main method as mentioned above.

After running the program the servlet shall start and the program is available by entering the following address in your browser URL bar:

```
localhost:9000
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc