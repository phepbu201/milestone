package wpd2.teame.milestonejava;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;

public class Main implements Serializable {

    Login login;

    public Main() {
        try {
            login = new Login();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() throws ParseException {
        login.run();    //Starts login screen
    }

    public static void main(String[] args) {
        try {
            Main m = new Main();
            m.start();      //Runs the app
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
