package wpd2.teame.milestonejava;

import java.text.ParseException;

public class Editor {

    private PlannerMenu menu;

    public Editor(PlannerMenu pm) {
        menu = pm;
    }

    public void displayEditMenu(Milestone ms) throws ParseException {   //Prints menu and prompts user to select option
        Milestone editing = ms;
        int menuOption = 0;
        do {
            System.out.println("Which field would you like to edit? \n1: Title \n2: Description \n3: Due Date " +
                    "\n4: Completion Date \n5: Finish editing");

            menuOption = menu.checkOption(menuOption, 1, 5);

            switch(menuOption) {
                case 1:
                    editTitle(editing);
                    break;

                case 2:
                    editDescription(editing);
                    break;

                case 3:
                    editDueDate(editing);
                    break;

                case 4:
                    editCompletionDate(editing);
                    break;

                case 5:
                    menu.displayPlannerMenu();

                default:
                    System.out.println("Invalid option. Please enter a number between 1 and 5");
                    displayEditMenu(editing);
            }
        } while (menuOption != 5);  //Loops until valid option is selected
    }

    public void editTitle(Milestone ms) {   //Replaces selected milestone title
        System.out.println("Editing Title. \nCurrent Title: " + ms.getTitle());
        System.out.println("\nNew Title:");
        System.out.print(">>");
        ms.setTitle();
    }

    public void editDescription(Milestone ms) {   //Replaces selected milestone description
        System.out.println("Editing Description. \nCurrent Description: " + ms.getDescription());
        System.out.println("\nNew Description:");
        System.out.print(">>");
        ms.setDescription();
    }

    public void editDueDate(Milestone ms) {   //Replaces selected milestone due date
        System.out.println("Editing Due Date. \nCurrent Due Date: " + ms.getDueDate());
        System.out.println("\nNew Due Date:");
        System.out.print(">>");
        ms.setDueDate();
    }

    public void editCompletionDate(Milestone ms) {   //Replaces selected milestone completion date
        System.out.println("Editing Completion Date. \nCurrent Completion Date: " + ms.getCompletionDate());
        System.out.println("\nNew Completion Date:");
        System.out.print(">>");
        ms.setCompletionDate();
    }
}
