package wpd2.teame.milestonejava;

import javafx.util.Pair;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Login {

    private List<Pair> logins;
    private Pair<String, String> credentials;
    private Scanner sc;

    private Planner p;
    private PlannerMenu pm;

    private LoginFileStream ser;
    File f;

    public Login() throws IOException {
        sc = new Scanner(System.in);    //Scanner for reading user data

        ser = new LoginFileStream();    //initialise file stream to save/load users
        f = new File(ser.getLoginPath());   //Get file containing logins

        if(f.exists()) {
            logins = ser.readUsersFromFile();
        } else {
            System.out.println("User file not found, creating new...");
            logins = new ArrayList<>();
            f.createNewFile();
            System.out.println("User file created!");
        }
    }

    public void run() {
        int option = 0;

        System.out.println("1.Login / 2.Register / 3.Quit");

        option = checkOption(option);

        if (option == 1) {
            startLogin();
        } else if (option == 2){
            startReg();
        } else {
            System.out.println("Goodbye!");
            System.exit(0);
        }
    }


    public void startLogin() {

        if (logins.size() == 0) {
            System.out.println("No users registered! Please select register to start.");
        } else {
            System.out.print("Please enter your username:\n>>");
            String uname = sc.nextLine();

            System.out.print("Please enter your password:\n>>");
            String pword = sc.nextLine();

            for (Pair user : logins) {  //Loops through every user in login array

                if (user.getKey().equals(uname) && user.getValue().equals(pword)) {  //Check uname and pword match any stored users

                    try {
                        p = new Planner(uname + "'s Milestone Planner", user);  //Initialise user's planner on success
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    pm = new PlannerMenu(p, this);    //Initialise planner menu

                    try {
                        pm.displayPlannerMenu();    //Display users planner menu
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            }
            System.out.println("Invalid username or password. Please try again.");
            run();
        }
        run();
    }

    public void startReg() {
        String newUname, newPW;
        System.out.print("Welcome, new user!\nPlease enter your username:\n>>");
        newUname = sc.nextLine();

        System.out.print("\nPlease enter your password:\n>>");
        newPW = sc.nextLine();

        credentials = new Pair(newUname, newPW);    //Stores newUname and newPW as key, value pair

        logins.add(credentials);    //Add user to list of logins

        System.out.println("Saving new user...");
        ser.writeUserToFile(logins);


        try {
            p = new Planner(credentials.getKey() + "'s Milestone Planner", credentials);    //Initialise planner for new user
        } catch (IOException e) {
            e.printStackTrace();
        }
        pm = new PlannerMenu(p, this);

        try {
            pm.displayPlannerMenu();    //Display new users planner menu
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    private int checkOption(int opt) {  //Ensures user enters either 1 or 2 at login entry screen
        boolean valid = false;

        do {
            try {
                opt = sc.nextInt();
                if(opt >= 1 && opt <= 3) {
                    valid = true;
                } else {
                    System.out.println("Invalid option. Please enter a number between 1 and 3.");
                    sc.nextLine();
                }
            } catch (InputMismatchException exception) {

                System.out.println("Invalid option. Please enter a number between 1 and 3.");
                sc.nextLine();
            }
        } while (!valid);

        sc.nextLine();
        return opt;
    }
}
