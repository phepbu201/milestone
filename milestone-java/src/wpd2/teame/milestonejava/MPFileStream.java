package wpd2.teame.milestonejava;

import javafx.util.Pair;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class MPFileStream {

    private final String filepath;

    public MPFileStream(Pair<String, String> user) {
        filepath = user.getKey() + "milestoneplanner.dat";  //Sets file path to users planner file
    }

    public void writeToFile(List<Milestone> list) {

        try {
            FileOutputStream fileOut = new FileOutputStream(filepath);  //Initialise file output stream
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut); //Initialise object output stream

            objectOut.writeObject(list);    //Write planner to file

            objectOut.close();  //Close object output stream
            System.out.println("Save complete.");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (NoSuchElementException nsee) {
            nsee.printStackTrace();
        }
    }

    public List<Milestone> readFromFile() {
        List<Milestone> list = new ArrayList<>();   //Initialise new array to store milestones read from file

        try {
            FileInputStream fileIn = new FileInputStream(filepath); //Initialise file input stream on users planner file
            ObjectInputStream objectIn = new ObjectInputStream(fileIn); //Initialise object input stream to read file

            list = (ArrayList<Milestone>) objectIn.readObject();    //Assign read milestones to list

            objectIn.close();   //Close object input stream
            System.out.println("Load complete.");
        } catch (EOFException eofe) {
            return list;
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return list;
    }

    public String getPlannerPath() { return filepath; }
}
