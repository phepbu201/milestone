package wpd2.teame.milestonejava;

import javafx.util.Pair;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Planner implements Serializable {

    private final String plannerTitle;
    private List <Milestone> milestones;
    private MPFileStream ser;
    File f;

    public Planner(String title, Pair<String, String> user) throws IOException {
        plannerTitle = title;   //Assigned from login screen

        ser = new MPFileStream(user);   //Initialise milestone file stream for selected user
        f = new File(ser.getPlannerPath()); //Get file path for users milestones

        if(f.exists()) {
            milestones = ser.readFromFile();    //Loads users milestones
        } else {
            System.out.println("Milestone Planner not found, creating new...");
            milestones = new ArrayList<>();
            f.createNewFile();
            System.out.println("Milestone Planner created!");
        }
    }

    public MPFileStream getSer() {
        return ser;
    }   //returns file stream

    public void addMilestone(Milestone ms) { milestones.add(ms); }  //adds created milestone to list

    public Milestone getMilestone(int n) { return milestones.get(n); }  //retrieves milestone at list index n

    public List<Milestone> getMilestoneList() { return milestones; }    //returns full list of milestones

    public int getSizeOfMilestones() { return milestones.size(); } //returns size of milestone list

    public void remove(int n) { milestones.remove(n); } //Removes milestone at list index n

    public void display() { //Prints all milestone titles with numbers for selection at menu
        int count = 0;
        System.out.println(plannerTitle + "\n");

        for(Milestone ms : milestones) {
            count++;
            System.out.println("Milestone No: "+ count + "\t\tTitle: " + ms.getTitle());
        }
    }


    public void displayMilestones() {   //Print all milestones
        for(Milestone ms : milestones) { ms.displayMilestone(); }
    }
}
