package wpd2.teame.milestonejava;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Milestone implements Serializable {

    private String title;
    private String description;
    private Date dueDate;
    private Date completionDate;
    private SimpleDateFormat fmt;
    private transient Scanner scn;

    public Milestone() {
        fmt = new SimpleDateFormat("dd/MM/yyyy");
        scn = new Scanner(System.in);
    }

    public void displayMilestone() {    //Prints current milestone to console window
        System.out.println("Title: " + title + "\nDescription: " + description);
        System.out.println("Due Date: " + fmt.format(dueDate) + "\t\t Completion Date: " + fmt.format(completionDate));
    }

    public String getTitle() { return title; }  //returns milestone title
    public String getDescription() { return description; }  //returns milestone description
    public Date getDueDate() { return dueDate; }    //returns milestone duedate
    public Date getCompletionDate() { return  completionDate; } //returns milestone completion date


    //Assigns milestone title to user input from console
    public void setTitle() { this.title = scn.nextLine(); }

    //Assigns milestone description to user input from console
    public void setDescription() { this.description = scn.nextLine(); }

    public Date checkDate(String data) throws ParseException {    //Checks entered date is in valid format
        Date checked = fmt.parse(data);
        return checked;
    }

    //Assigns milestone due date to user input from console once checked
    public void setDueDate() {
        boolean valid = false;
        do {
            try {
                this.dueDate = checkDate(scn.nextLine());
                valid = true;
            } catch (ParseException e) {
                System.out.println("Invalid date, please use the format: DD/MM/YYYY");
            }
        } while(!valid);
    }

    //Assigns milestone completion date to user input from console once checked
    public void setCompletionDate() {
        boolean valid = false;
        do {
            try {
                this.completionDate = checkDate(scn.nextLine());
                valid = true;
            } catch (ParseException e) {
                System.out.println("Invalid date, please use the format: DD/MM/YYYY");
            }
        } while(!valid);
    }
}
