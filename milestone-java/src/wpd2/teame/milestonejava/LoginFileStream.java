package wpd2.teame.milestonejava;

import javafx.util.Pair;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class LoginFileStream {

    private static final String loginPath = "logins.dat";

    public void writeUserToFile(List<Pair> users) {

        try {
            FileOutputStream userOut = new FileOutputStream(loginPath);
            ObjectOutputStream objectOut = new ObjectOutputStream(userOut);

            objectOut.writeObject(users);

            objectOut.close();
            System.out.println("Save complete.");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (NoSuchElementException nsee) {
            nsee.printStackTrace();
        }
    }

    public List<Pair> readUsersFromFile() {
        List<Pair> users = new ArrayList<>();

        try {
            FileInputStream fileIn = new FileInputStream(loginPath);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            users = (ArrayList<Pair>) objectIn.readObject();

            objectIn.close();
            System.out.println("Load complete.");
        } catch (EOFException eofe) {
            return users;
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return users;
    }

    public String getLoginPath() { return loginPath; }
}
