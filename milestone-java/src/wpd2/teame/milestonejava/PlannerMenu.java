package wpd2.teame.milestonejava;

import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class PlannerMenu {

    private Planner planner;
    private Editor editor;
    private Login log;

    public PlannerMenu(Planner p, Login l) {
        planner = p;    //Load planner object for user specified at login
        editor = new Editor(this);  //Initialise Editor class
        log = l;
    }

    private static Scanner sc = new Scanner(System.in);

    public void displayPlannerMenu() throws ParseException {
        planner.display();

        int option = 0;
        do {
            System.out.println("\nMain Menu:");
            System.out.println("\n1: Add new milestone");
            System.out.println("2: Display specific milestone");
            System.out.println("3: Display all milestones");
            System.out.println("4: Remove milestone");
            System.out.println("5: Edit milestone");
            System.out.println("6: Share milestone");
            System.out.println("7: Logout");
            System.out.println("8: Exit");

            option = checkOption(option, 1, 7);

            switch(option) {
                case 1:
                    addToMilestones();
                    break;

                case 2:
                    displayMilestone();
                    break;

                case 3:
                    planner.displayMilestones();
                    break;

                case 4:
                    removeMilestone();
                    break;

                case 5:
                    editMilestone();
                    break;

                case 6:
                    shareMilestone();
                    break;

                case 7:
                    logout();
                    break;

                case 8:

                    System.out.println("Saving Milestone Planner...");
                    planner.getSer().writeToFile(planner.getMilestoneList());   //Use file stream from planner to write milestones for user

                    System.out.println("Closing appliction...");
                    System.exit(0); //Close app
                    break;

                default:
                    System.out.println("Invalid option. Please enter a number between 1 and 5");
                    displayPlannerMenu();
            }
        } while(option != 7);   //Loops through these options until user selects exit
    }

    public void addToMilestones() throws ParseException {

        Milestone ms = new Milestone(); //Initialise new milestone object to be added to array

        System.out.println("Enter the title of your milestone: ");
        ms.setTitle();

        System.out.println("Enter the description of your milestone: ");
        ms.setDescription();

        System.out.println("Enter the due date of your milestone: ");
        ms.setDueDate();

        System.out.println("Enter the completion date of your milestone: ");
        ms.setCompletionDate();


        planner.addMilestone(ms);   //Add new milestone to array
        displayPlannerMenu();
    }

    public void displayMilestone() {    //Displays selected milestone if array is not empty
        int option = 0;

        if (planner.getSizeOfMilestones() == 0) {
            System.out.println("\nNo milestones available. Add some first to view them individually.");

        } else {
            System.out.println("Please enter the number of the milestone you'd like to view: ");

            option = checkOption(option, 1, planner.getSizeOfMilestones());

            planner.getMilestone(option -1).displayMilestone();
        }
    }

    public void removeMilestone() { //Deletes selected milestone if array is not empty
        int option = 0;

        if (planner.getSizeOfMilestones() == 0) {
            System.out.println("\nNo milestones available for deletion.");

        } else {
            System.out.println("Please enter the number of the milestone to be deleted.");

            option = checkOption(option, 1, planner.getSizeOfMilestones());

            planner.remove(option - 1);

            System.out.println("Milestone " + option + " deleted.");
        }
    }

    public void editMilestone() throws ParseException { //Allows user to edit selected milestone if array is not empty
        Milestone toEdit;
        int option = 0;

        if (planner.getSizeOfMilestones() == 0) {
            System.out.println("\nNo milestones available for editing.");

        } else {
            System.out.println("Please enter the number of the milestone to be edited.");

            option = checkOption(option, 1, planner.getSizeOfMilestones());

            toEdit = planner.getMilestone(option - 1);  //Assigns milestone to be edited to variable to be passed to Editor

            System.out.println("Milestone " + option + " selected");

            editor.displayEditMenu(toEdit); //Runs Editor for selected milestone
        }
    }


    public void shareMilestone() { }    //Not sure if I can implement this from console app

    public void logout() {
        System.out.println("Saving Milestone Planner...");
        planner.getSer().writeToFile(planner.getMilestoneList());   //Use file stream from planner to write milestones for user

        System.out.println("Logging out...");

        log.run();
    }


    public int checkOption(int opt, int n1, int n2) {   //Checks int is between specified range
        int min = n1;
        int max = n2;
        boolean valid = false;

        do {
            try {
                opt = sc.nextInt();
                if(opt >= min && opt <= max) {
                    valid = true;
                } else {
                    System.out.println("Invalid option. Please enter a number between " + min + " and " + max);
                    sc.nextLine();
                }
            } catch (InputMismatchException exception) {

                System.out.println("Invalid option. Please enter a number between " + min + " and " + max);
                sc.nextLine();
            }
        } while (!valid);

        sc.nextLine();
        return opt;
    }
}
