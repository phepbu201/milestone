// Copyright (c) 2018 Katrin Hartmann. All Rights Reserved.
//
// File:        IMessageDB.java
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package teame.milestone.db;

import teame.milestone.model.Milestone;

import java.util.List;

public interface IMilestoneDB {
    /**
     * Get milestone with given id
     * @param id the id to look up
     * @return the milestone or null if there is none
     */
    Milestone get(long id);

    /**
     * Add a message from a given user.  The timestamp will be <code>new Date().getTime()</code>
     * @param name  Message text (not null)
     * @param description  Message text (not null)
     * @param dueDate  Message text (not null)
     * @param completionDate  Message text (not null)
     * @param user User name (not null)
     */
    void add(String name, String description, String dueDate, String completionDate, String user);

    /**
     * List all milestones
     * @return A list of all milestones, or an empty list if there are none
     */
    List<Milestone> list();

    /**
     * List messages for a given user
     * @param user  User name (not null)
     * @return  The messages from this user, or the empty list if none
     */
    List<Milestone> user(String user);

    /**
     * Delete message with the given id, if it exists
     * @param id The id
     */
    void delete(long id);

    /**
     * Edit milestone with given id by replacing fields with entered values
     * @param name milestone name (not null)
     * @param description Milestone description (not null)
     * @param dueDate Milestone date to be achieved by (not null)
     * @param completionDate Milestone actual completion date
     */
    void edit(long id, String name, String description, String dueDate, String completionDate);
}