
package teame.milestone.guice;


        import com.google.inject.AbstractModule;
        import com.google.inject.Scopes;
        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import teame.milestone.db.*;
        import teame.milestone.servlet.*;


class BindingModule extends AbstractModule {
    @SuppressWarnings({"unused"})
    static final Logger LOG = LoggerFactory.getLogger(BindingModule.class);

    BindingModule() {}

    @Override
    protected void configure() {
        // rather than bind the servlets you can add an <code>@Singleton</code> annotation
        // just before the class declaration (see <code>LoginServlet</code>).
        bind(LoginServlet.class).in(Scopes.SINGLETON);
        bind(LogoutServlet.class).in(Scopes.SINGLETON);
        bind(MilestoneServlet.class).in(Scopes.SINGLETON);
        bind(DeleteServlet.class).in(Scopes.SINGLETON);
        bind(EditServlet.class).in(Scopes.SINGLETON);

        bind(H2User.class).toInstance(new H2User(new ConnectionSupplier(ConnectionSupplier.FILE)));
        bind(IMilestoneDB.class).toInstance(new H2Milestone(new ConnectionSupplier(ConnectionSupplier.FILE)));
    }
}
