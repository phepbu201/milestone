package teame.milestone.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import teame.milestone.db.IMilestoneDB;
import teame.milestone.model.Milestone;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class MilestoneServlet extends BaseServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(MilestoneServlet.class);

    private static final String MILESTONE_TEMPLATE = "milestones.mustache";
    private static final long serialVersionUID = 687117339002032958L;

    private final IMilestoneDB db;

    @Inject
    public MilestoneServlet(IMilestoneDB dataB) { this.db = dataB; }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!authOK(request, response)) {
            return;
        }
        List<Milestone> milestones = db.user(UserFuncs.getCurrentUser(request));
        Map<String, Object> map = new HashMap<>();
        String userName = UserFuncs.getCurrentUser(request);
        map.put("user", userName);
        if (milestones.size() > 0) {
            map.put("milestones", milestones);
        }
        showView(response, MILESTONE_TEMPLATE, map);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("pname");
        String desc = request.getParameter("pdesc");
        String due = request.getParameter("pdue");
        String comp = request.getParameter("pcomp");
        String user = UserFuncs.getCurrentUser(request);
        db.add(name, desc, due, comp, user);
        response.sendRedirect("/milestone");
    }
}
