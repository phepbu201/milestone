package teame.milestone.servlet;

import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import teame.milestone.db.IMilestoneDB;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Singleton
public class EditServlet extends BaseServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(EditServlet.class);

    private final IMilestoneDB db;

    @Inject
    public EditServlet(IMilestoneDB dataB) { this.db = dataB; }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String editID = request.getParameter("ms-id");
        long id =Long.parseLong(editID);

        String name = request.getParameter("ms-name");
        String desc = request.getParameter("ms-desc");
        String due = request.getParameter("ms-due");
        String comp = request.getParameter("ms-comp");
        db.edit(id, name, desc, due, comp);
        response.sendRedirect("/milestone");
    }
}
