package teame.milestone.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import teame.milestone.db.IMilestoneDB;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Singleton
public class DeleteServlet extends BaseServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(DeleteServlet.class);

    private final IMilestoneDB db;

    @Inject
    public DeleteServlet(IMilestoneDB dataB) { this.db = dataB; }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String deleteID = request.getParameter("ms-delete");
        long id = Long.parseLong(deleteID);
        db.delete(id);
        response.sendRedirect("/milestone");
    }
}
