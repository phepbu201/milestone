$(document).ready(function(){
    var $form = $(".edit-div");
    var $id = $("#ms-id");
    var $name = $("#ms-name");
    var $description = $("#ms-desc");
    var $dueDate = $("#ms-due");
    var $completionDate = $("#ms-comp");
    var $hiddenID = $("#hidden-id");
    var $closeBtn = $("#close-btn");
    var $deleteId = $("#ms-delete");

    $("table:visible").delegate("tr.milestone-row", "click", function(){
        var msId = $(this).find(".id-cell").html();
        var msName = $(this).find(".name-cell").html();
        var msDesc = $(this).find(".desc-cell").html();
        var msDue = $(this).find(".due-cell").html();
        var msComp = $(this).find(".comp-cell").html();

        $($id).text("Milestone ID: " + msId);
        $($hiddenID).val(msId);
        $($name).val(msName);
        $($description).val(msDesc);
        $($dueDate).val(msDue);
        $($completionDate).val(msComp);

        $($deleteId).val(msId);
        console.log($deleteId.val());

        $($form).removeClass("hidden");

    });

    $($closeBtn).click(function () {
        $($form).addClass("hidden");
    });

});